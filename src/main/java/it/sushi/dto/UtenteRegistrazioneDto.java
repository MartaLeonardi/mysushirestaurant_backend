package it.sushi.dto;

import jakarta.validation.constraints.Pattern;

public class UtenteRegistrazioneDto {

	@Pattern(regexp = "[A-Za-zàèìòù ]{1,50}", message = "Nome con caratteri non ammessi.")
	private String nome;
	
	@Pattern(regexp = "[A-Za-zàèìòù ]{1,50}", message = "Cognome con caratteri non ammessi.")
	private String cognome;
	
	@Pattern(regexp = "[0-9]{10}", message = "Telefono con caratteri non ammessi.")
	private String telefono;
	
	@Pattern(regexp = "[A-z0-9\\._-]+@[A-z0-9\\._-]+\\.[A-z]{2,8}", message = "Email con caratteri non ammessi.")
	private String email;
	
	private String password;

	private String ruolo;
	
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRuolo() {
		return ruolo;
	}

	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}
	

	
}

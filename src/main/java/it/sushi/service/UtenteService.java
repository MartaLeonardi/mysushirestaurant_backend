package it.sushi.service;

import it.sushi.dto.UtenteRegistrazioneDto;

public interface UtenteService {

	// Metodo per la registrazione dell'utente
	void userRegistration(UtenteRegistrazioneDto utenteDto);
	
	// Metodo per verificare se esiste l'utente tramite email
	boolean existsUtenteByEmail(String email);
	
}

package it.sushi.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.sushi.dao.UtenteDao;
import it.sushi.dto.UtenteRegistrazioneDto;
import it.sushi.model.Utente;

@Service
public class UtenteServiceImpl implements UtenteService {

	@Autowired
	private UtenteDao utenteDao;
	
	@Override
	public void userRegistration(UtenteRegistrazioneDto utenteDto) {
		
		Utente utente = new Utente();
		
		//setto i campi inseriti dall'utente
		utente.setNome(utenteDto.getNome());
		utente.setCognome(utenteDto.getCognome());
		utente.setTelefono(utenteDto.getTelefono());
		utente.setEmail(utenteDto.getEmail());
		utente.setRuolo("Utente");
		
		//si applica l'hashing alla password immessa dall'utente
		String sha256hex = DigestUtils.sha256Hex(utenteDto.getPassword());
		
		//setto il campo della password
		utente.setPassword(sha256hex);
		
		utenteDao.save(utente);

	}
	
	

	@Override
	public boolean existsUtenteByEmail(String email) {
		return utenteDao.existsByEmail(email);
	}
	
}

package it.sushi.dao;


import org.springframework.data.repository.CrudRepository;

import it.sushi.model.Utente;

public interface UtenteDao extends CrudRepository<Utente, Integer> {
	
	boolean existsByEmail(String email);
	Utente findByEmailAndPassword(String email, String password);
	Utente findByEmail(String email);
	
}

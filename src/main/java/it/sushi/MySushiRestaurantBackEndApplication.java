package it.sushi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MySushiRestaurantBackEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(MySushiRestaurantBackEndApplication.class, args);
	}

}

package it.sushi.controller;

import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import it.sushi.dto.UtenteRegistrazioneDto;
import it.sushi.service.UtenteService;
import jakarta.validation.Valid;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/utente")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UtenteController {

	@Autowired
	private UtenteService utenteService;
	
	@POST
	@Path("/registrazione")
	public Response userRegistration( @Valid @RequestBody UtenteRegistrazioneDto utenteDto ) {
		
		try {
			
			// Validazione password
			if( !Pattern.matches("(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@.#$€!%*?&^_])[A-Za-z\\d@.#$€!%*?&_]{6,20}", utenteDto.getPassword()) ) {
				
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
			
			// Verifico se esiste già l'utente attraverso l'email
			if( utenteService.existsUtenteByEmail(utenteDto.getEmail())) {
				
				return Response.status(Response.Status.BAD_REQUEST).build();
				
			}
			
			utenteService.userRegistration(utenteDto);
			return Response.status(Response.Status.OK).build();
			
		} catch (Exception e) {
			
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
	}
	
}
